# Kustomize - Logging Stack

A flexible set of Kustomize `Component`s to easily build the logging stack best suited for your environment's needs.
